
import {
  currencyIsoCodesManifest,
  currentSourceCurrencyIsoCode,
  initialTargetCurrencyIsoCode,
} from '../constants';



/**
 * This reducer is optimized for readability over performance.
 * Should larger data sets be needed, a single for i in length
 * loop would be used.
 *
 * As this list is likely only parsed infrequently, I went with this style.
 *
 * I attempted first to use the graphql where: in operator to reduce the work
 * that would be needed here.  This seems not to be operating in everbase as I expected.
 * For sake of deadline, I filter here.
 *
 * ALSO ..  later add something to proper case all the currency names.
 * @param state
 * @param currenciesInfo
 * @constructor
 */
export const SET_CURRENCIES = ( state, { currencies } ) => {
  const list =
    reshapeCurrenciesInfo(
      filterCurrenciesByManifest( currencies )
    ).sort( ( a, b ) => a.name.toUpperCase() > b.name.toUpperCase() ? 1 : -1 );
  const isoCodeIndex = makeCurrencyIsoCodeIndex( { list } );
  const isoCodeList = makeCurrencyIsoCodeList( { list } );
  const targetList = excludeFromCurrencyList( { list }, 'USD' );
  const nameList = makeCurrencyNameList( { list } );
  const source = getCurrencyByIsoCode( { list, isoCodeIndex }, currentSourceCurrencyIsoCode );
  const target = getCurrencyByIsoCode( { list, isoCodeIndex }, initialTargetCurrencyIsoCode );

  console.log( JSON.stringify( list ));

  state.currencies = {
    ...state.currencies,
    allCurrencies: currencies, // canonical api call data
    list,
    isoCodeList,
    isoCodeIndex,
    nameList,
    source: {
      ...source,
      amount: 1.00,
    },
    target: {
      ...target,
      amount: 0,
      list: targetList,
    },
  };
};

export const SWITCH_TARGET_CURRENCY = async ( state, { isoCode }) => {
  const { currencies } = state;
  let target =
    getCurrencyByIsoCode( currencies, isoCode );
  const list =
    excludeFromCurrencyList( currencies, [ ...'USD', isoCode ] );

  target = {
    ...state.target,
    ...target,
    list,
  };

  state.currencies = {
    ...state.currencies,
    target
  }
};

export const CHANGE_CURRENCY_AMOUNTS = ( state, { source, target } ) => {
  state.currencies = {
    ...state.currencies,
    source: { ...state.source, ...source },
    target: { ...state.target, ...target },
  };
};

const getCurrencyByIsoCode = ( { list, isoCodeIndex }, isoCode ) =>
  list[ isoCodeIndex[ isoCode ] ];

const excludeFromCurrencyList = ( { list }, isoCodes ) =>
  list.filter( currency => !isoCodes.includes( currency.isoCode ) )
    .sort( ( a, b ) => a.name.toUpperCase() > b.name.toUpperCase() ? 1 : -1 );

const makeCurrencyIsoCodeIndex = ( { list } ) => {
  const index = {};
  list.forEach(
    ( currency, i ) => {
      index[ currency.isoCode ] = i;
    }
  );
  return index;
};

const makeCurrencyIsoCodeList = ( { list } ) => {
  return list.map( currencyInfo => currencyInfo.isoCode );
};

const makeCurrencyNameList = ( { list } ) => {
  return list.map( currencyInfo => currencyInfo.name );
};

const filterCurrenciesByManifest = currenciesInfo =>
  currenciesInfo.filter( currencyInfo =>
    currencyIsoCodesManifest.includes( currencyInfo.isoCode ) );

const reshapeCurrenciesInfo = currenciesInfo =>
  currenciesInfo.map( reshapeCurrencyInfo );

const reshapeCurrencyInfo = currencyInfo =>
  ( { isoCode: currencyInfo.isoCode, name :currencyInfo.name } );


