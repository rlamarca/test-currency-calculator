import Vue from "vue";
import Vuex from "vuex";
import gql from "graphql-tag";


import {
  CHANGE_CURRENCY_AMOUNTS,
  SET_CURRENCIES,
  SWITCH_TARGET_CURRENCY,
} from './currencies.mutations';
import {
  changeSourceAmount,
  fetchCurrencies,
  recalcTargetAmount,
  switchTargetCurrency,
} from './currencies.actions';
import { apolloClient } from '../apollo';


Vue.use(Vuex);

const state = () => ({
  siteTitle: 'Currency Conversion App.',
  currencies: {
    allCurrencies: [], // canonical api call data
    list: [],
    isoCodeList: [],
    isoCodeIndex: [],
    nameList: [],
    source: {
      name: '',
      isoCode: '',
      amount: 1.00,
    },
    target: {
      name: '',
      isoCode: '',
      amount: 0,
      list: [],
    },
  },
});

const actions = {
  changeSourceAmount,
  fetchCurrencies,
  recalcTargetAmount,
  switchTargetCurrency,
};

const mutations = {
  CHANGE_CURRENCY_AMOUNTS,
  SET_CURRENCIES,
  SWITCH_TARGET_CURRENCY,
};

const modules = {};


export default new Vuex.Store( {
  actions, modules, mutations, state,
})


