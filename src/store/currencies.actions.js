import {apolloClient} from '../apollo';
import gql from "graphql-tag";


export const fetchCurrencies = async ({ commit }) => {
  const response = await apolloClient.query( {
    query: gql`query {
        currencies {
          name
          isoCode
        }
      }`,
  });

  const { currencies } = response.data;
  commit( 'SET_CURRENCIES', { currencies });
};

export const switchTargetCurrency = ( { commit }, { isoCode } ) => {
  commit( 'SWITCH_TARGET_CURRENCY', { isoCode });
};


export const changeSourceAmount = async ( { commit }, { amount, source, target } ) => {
  source = {
    ...source,
    amount,
  };

  target = await _recalcTargetAmount( { source, target } );
  commit( 'CHANGE_CURRENCY_AMOUNTS', {  source, target } );
};

export const recalcTargetAmount = async ( { commit }, { source, target } ) => {
  target = await _recalcTargetAmount( { source, target });
  commit( 'CHANGE_CURRENCY_AMOUNTS', { source, target });
};

const _recalcTargetAmount = async ( { source, target } ) => {
  const { amount } = source;
  const query = gql`
    query { 
      currencies( where: { isoCode: { eq: "${source.isoCode}" } } ) { 
        to: convert( amount: ${amount}, to: "${target.isoCode}" ) 
      }
    }
  `;
  const response = await apolloClient.query( { query } );
  return {
    ...target,
    amount: response.data.currencies[ 0 ].to,
  };

};
