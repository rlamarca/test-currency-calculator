
export const currencyComponentMethods = {
  _sg( key, dflt = null ) {
    return this.$store.state.currencies[ key ] || dflt;
  }
};
