
import ApolloClient from 'apollo-boost';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { API_URI } from './constants';

const apolloCache = new InMemoryCache();

export const apolloClient = new ApolloClient({
  cache: apolloCache,
  uri: API_URI,
});
