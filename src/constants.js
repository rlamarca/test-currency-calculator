
export const APP_TITLE = 'Currency Conversion';

export const API_URI = 'https://api.everbase.co/graphql?apikey=alpha';

export const currencyIsoCodesManifest = [
  'BRL',
  'CAD',
  'EUR',
  'JPY',
  'MXN',
  'USD',
];



export const currentSourceCurrencyIsoCode = 'USD';
export const initialTargetCurrencyIsoCode = 'EUR';
