import Vue from 'vue';
import Vuex from 'vuex';
import App from './App.vue';
import store from './store';

import VueApollo from 'vue-apollo';
import { apolloClient } from './apollo';
import VueSelect from 'vue-select';


Vue.config.productionTip = false;
Vue.use( VueApollo );
Vue.use( Vuex );
Vue.component( 'v-select', VueSelect );

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
});

new Vue({
  store,
  render: h => h(App),
  apolloProvider,
}).$mount('#app');
