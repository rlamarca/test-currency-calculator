# nodo-vue-test

## Notes ( setup and yarn commands below):
 
I created this in Vue using Vuex as the primary store and Apollo-boost
as the foundation of the graphql.  

Were it not for time constraints, I would have considered using the 
apollo store more widely, perhaps even instead of the vuex.

I would have also possibly provided more graphql components.

Tests are limited, but these would have been built out more.

Since the project notes specified a readonly source currency of the US Dollar,
I found it easier to create separate components for the Currency Source 
and the CurrencySelector.

Were the project to call for the user to select both source and target 
currencies, I would have likely created a single Currency Selector
differentiated by input parameters.

In this case, creating a single component with modes was less 
readable than two separate components.
 


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
