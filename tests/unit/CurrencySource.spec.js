import { shallowMount } from "@vue/test-utils";
import CurrencySource from "@/components/CurrencySource.vue";

import {
  mockCurrencyList,
  mockSource,
  mockTargetList,
} from '../mock-data';

describe('CurrencySource.vue',
  () => {

  let wrapper;

  beforeEach( () => {
    wrapper = shallowMount( CurrencySource, {
      propsData: { source: mockSource }
    });
  });

  it(`renders props.source.name title when passed`, () => {
    const expectedTitle = `Convert from United States dollar`;
    expect(wrapper.text()).toMatch( expectedTitle );
  });

  it( `should have an input field with a value of 1`,
    () => {
      const inputElement = wrapper.find( 'input' ).element;
      expect( inputElement ).toBeTruthy();
      console.log( inputElement );
      expect( inputElement.value ).toBe( String( mockSource.amount ) );
    }
  );


});
