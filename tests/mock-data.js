
export const mockCurrencyList = [
  { isoCode:"BRL", name: "Brazilian real" },
  { isoCode:"CAD", name: "Canadian dollar" },
  { isoCode:"EUR", name: "euro" },
  { isoCode:"JPY", name: "Japanese yen" },
  { isoCode:"MXN", name: "Mexican peso" },
  { isoCode:"USD", name: "United States dollar"}
];

export const mockTargetList = [
  { isoCode:"BRL", name: "Brazilian real" },
  { isoCode:"CAD", name: "Canadian dollar" },
  { isoCode:"EUR", name: "euro" },
  { isoCode:"JPY", name: "Japanese yen" },
  { isoCode:"MXN", name: "Mexican peso" },
];

export const mockSource = {
  amount: 1,
   isoCode: "USD",
   name: "United States dollar"
};


